# WhatToPack
## A cross-platform React-Native application, helping you pack for your voyages.

**Key words:** Trips, Travels, Voyages, Mobile application, JavaScript, React-Native, Android, iOS, Windows10

### Installation (side load):
 - Android: [app-release.apk](https://gitlab.com/MikeDabrowski/what-to-pack/blob/master/release/android/app-release.apk)
 - Windows 10 Desktop: [test4 ReleaseBundle](https://gitlab.com/MikeDabrowski/what-to-pack/blob/master/release/windows/test4_1.0.6.0_ReleaseBundle_Test.zip)
###### Note: To install on windows run .ps1 script after extracting package.

### Expo
Coming soon

### Build from source
 - Android/iOS: clone project and `react-native run-android` or `react-native run-ios`
 - Windows: coming soon