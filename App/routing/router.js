import React from 'react';

import { StackNavigator, TabNavigator } from 'react-navigation';
import NavigationActions from 'react-navigation/lib-rn/NavigationActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { store } from '../app';
import * as actions from '../redux/actions/actions';
import NewTrip from "../screens/NewTrip";
import PackingList from "../screens/PackingList";

export const Tabs = TabNavigator({
  NewTrip: {
    screen: NewTrip,
    navigationOptions: {
      tabBarLabel: 'Zaplanuj podróż',
      swipeEnabled: false
    }
  }, PackingList: {
    screen: PackingList,
    navigationOptions: {
      tabBarLabel: 'Twoja lista'
    }
  },
}, {
  initialRouteName: 'NewTrip',
  lazy: true
});

export const Root = StackNavigator(
  {
    Tabs: {
      screen: Tabs
    }
  },
  {
    headerMode: 'none',
    initialRouteName: 'Tabs'
  }
);

const defaultGetStateForAction = Root.router.getStateForAction;

Root.router.getStateForAction = (action, state) => {
  if(action.type === NavigationActions.NAVIGATE){
    if ((action.routeName === 'PackingList') &&
      !store.getState().default.canGenList) {
      console.log('blocking access to that tab for now');
      return null;
    }
  }

  return defaultGetStateForAction(action, state);
};


export default connect(state => ({
    state: state.WhatToPack
  }),
  (dispatch) => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(Root);