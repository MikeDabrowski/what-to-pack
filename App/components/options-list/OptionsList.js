import React, { Component } from 'react';
import { Animated, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import IconE from 'react-native-vector-icons/Entypo';
import IconFe from 'react-native-vector-icons/Feather';
import IconFA from 'react-native-vector-icons/FontAwesome';
import IconFo from 'react-native-vector-icons/Foundation';
import IconI from 'react-native-vector-icons/Ionicons';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';
import fontelloConfig from '../../../assets/custom-icons/config';

const CustomIcon = createIconSetFromFontello(fontelloConfig);

export default class OptionsList extends Component {
  handlePress = (key) => {
    this.props.onPress(key);
    console.log('pressing ' + key)
  };

  constructor(props) {
    super(props);
    this.state = {
      fadeAnim: new Animated.Value(0),
    }
  }

  componentDidMount() {
    /*Animated.timing(                  // Animate over time
        this.state.fadeAnim,            // The animated value to drive
        {
          toValue: 1,                   // Animate to opacity: 1 (opaque)
          duration: 10000,              // Make it take a while
        }
    ).start();                        // Starts the animation*/
  }

  isSelected(item) {
    return this.props.selected && this.props.selected.some((e) => e === item);
  }

  renderIcon(item) {
    const iconName = item.iconName;
    const style = [styles.icon, this.isSelected(item.id) ? styles.selected : null];
    switch (item.iconSet) {
      case 'ionicons':
        return (<IconI size={50} style={style} name={iconName}/>);
      case 'feather':
        return (<IconFe size={50} style={style} name={iconName}/>);
      case 'fontAwesome':
        return (<IconFA size={50} style={style} name={iconName}/>);
      case 'materialCommunityIcons':
        return (<IconMCI size={50} style={style} name={iconName}/>);
      case 'entypo':
        return (<IconE size={50} style={style} name={iconName}/>);
      case 'foundation':
        return (<IconFo size={50} style={style} name={iconName}/>);
      case 'custom':
        return (<CustomIcon size={50} style={style} name={iconName}/>);
      default:
        return (<IconFA size={50} style={style} name={iconName}/>);
    }
  }

  renderItem(item, itemKey) {
    return (
      <TouchableOpacity onPress={() => this.handlePress(item.id)}
                        key={itemKey}
                        style={styles.item}>
        {this.renderIcon(item)}
        <Text>{item.name}</Text>
      </TouchableOpacity>
    )
  }

  renderRow(row, rowKey) {
    return (<View style={styles.row} key={rowKey}>
        {row.value.map((rowItem, itemKey) => {
          return (this.renderItem(rowItem, itemKey))
        })}
      </View>
    )
  }

  prepData(oneDArray, rowLen) {
    let twoDArray = [], i = 0;
    while (oneDArray.length) {
      twoDArray.push({
        key: i,
        value: oneDArray.splice(0, rowLen)
      });
      i++;
    }
    return twoDArray;
  };

  renderList(listData, columns) {
    const formattedData = this.prepData(listData, columns);
    return (
      formattedData.map((row, rowKey) => {
        return (this.renderRow(row, rowKey))
      })
    )
  }

  isValid() {
    const list = this.props.store.default.listItems[this.props.listName];
    return this.props.submitted ? list && list.length : true;
  }

  render() {
    return (
      <View style={[styles.rowList, this.isValid() ? null : styles.invalid]}>
        {this.renderList(this.props.data, this.props.columns)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rowList: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: 'transparent',
    borderRadius: 4,
    width: '100%'
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  item: {
    width: 100,
    height: 100,
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
  },
  icon: {
    alignSelf: 'center',
    color: 'black',
  },
  selected: {
    color: 'green'
  },
  invalid: {
    borderColor: 'red'
  }
});