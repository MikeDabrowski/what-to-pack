import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../redux/actions/actions'
import OptionsList from "./OptionsList";
import BasicOptionsContainer from '../basic-options/BasicOptions.container';

class OptionsListComponent extends Component {

  handlePress = (item) => {
    const listItems = this.props.store.default.listItems;
    const listName = this.props.data.name;
    const listType = this.props.data.type;
    if (listType === 'multiSelect') {
      const index = listItems[listName] && listItems[listName].findIndex((el) => el === item);
      if (index > -1) {
        this.props.actions.removeItemFromList(listName, item);
      } else {
        this.props.actions.addItemToList(listName, item);
      }
    } else if (listType === 'singleSelect') {
      this.props.actions.emptyList(listName);
      this.props.actions.addItemToList(listName, item);
    }
    console.log(this.props.store.default);
  };

  isValid() {
    const list = this.props.store.default.listItems[this.props.data.name];
    return this.props.submitted ? list && list.length : true;
  }

  render() {
    return (
      <View flex={1} style={[styles.basic, this.isValid() ? null : styles.invalid]}>
        <Text style={styles.header}>{this.props.data.question || 'Simple list'}</Text>
        <OptionsList onPress={(item) => this.handlePress(item)} data={Object.values(this.props.data.values)}
                     columns={3} selected={this.props.store.default.listItems[this.props.data.name]}
                     submitted={this.props.submitted}
                     store={this.props.store}
                     listName={this.props.data.name}/>
        {
          this.props.data.name === 'genders' &&
          <BasicOptionsContainer submitted={this.props.submitted}/>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  basic: {
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15
  },
  header: {
    fontSize: 25
  },
  invalid: {
    paddingRight: 20,
    paddingLeft: 20
  }
});

function mapStateToProps(state, ownProps) {
  return {store: state}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OptionsListComponent);
