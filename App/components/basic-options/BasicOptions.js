import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../redux/actions/actions'


class BasicOptions extends Component {

  isValid() {
    return this.props.submitted ? this.props.data.age && this.props.data.days : true;
  }

  render() {
    return (
      <View flex={1} style={[styles.default, this.isValid() ? null : styles.invalid]}>
        <View style={styles.labelRow}>
          <Text style={[styles.column, styles.columnLeft]}>Wiek</Text>
          <TextInput maxLength={3} ref="age"
                     style={[styles.column, styles.columnRight, styles.textField, {textAlign: 'center'}]}
                     keyboardType='numeric'
                     value={this.props.data.age}
                     onChangeText={(age) => this.props.handleInput('age', age)}/>
        </View>
        <View style={[styles.formRow]}>
          <Text style={[styles.column, styles.columnLeft]}>Na ile dni jedziesz?</Text>
          <TextInput maxLength={3} ref="days"
                     style={[styles.column, styles.columnRight, styles.textField, {textAlign: 'center'}]}
                     keyboardType='numeric'
                     value={this.props.data.days}
                     onChangeText={(days) => this.props.handleInput('days', days)}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  /* ROWS */
  default: {
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  headerRow: {
    marginVertical: 20,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  labelRow: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  formRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },

  /* FIELDS */
  column: {
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'center',
    flexBasis: '20%'
  },
  columnLeft: {
    marginRight: 5
  },
  columnRight: {
    marginLeft: 5,
    width: 60
  },
  textField: {
    fontSize: 25,
    marginTop: -10,
    height: 55
  },
  invalid: {
    borderWidth: 2,
    borderColor: 'red',
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 4,
    width: '100%'
  }
});


function mapStateToProps(state, ownProps) {
  return {store: state}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BasicOptions);
