import React, { Component } from 'react';
import { Button, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../redux/actions/actions'


class PrepareButton extends Component {

  betaTests = true;

  render() {
    return (
      <View flex={1} style={styles.row}>
        <Button title="Przygotuj listę" onPress={this.props.onClick}/>
        {
          Object.keys(this.props.store.default.list).length > 0 &&
          <Button color={'red'} title="Nowa wycieczka" onPress={this.props.onReset}/>
        }
        {
          this.betaTests &&
          <Button color={'green'} title="Show input" onPress={this.props.modal}/>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end'
  }
});


function mapStateToProps(state, ownProps) {
  return {store: state}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PrepareButton);
