import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { store } from '../app';
import * as data from '../data/data';
import { categories } from '../data/data';
import * as Actions from '../redux/actions/actions';

class myList {
  constructor(data, initialData) {
    this._list = data;
    this._textParameters = initialData.textOptions;
    this._selectedParameters = initialData.selectedParameters;
    this._categories = initialData.categories;
  }

  hasCountableProps(element) {
    return Object.keys(this._selectedParameters).some(prop => Object.keys(element)
      .includes(prop));
  }
  matches(element) {
    const original = data.items.values.find(e => e.id === element.id);
    let isMatching = 0;
    if (original) {
      if(this.hasCountableProps(original)){
        for (let prop in original) {
          if(original.hasOwnProperty(prop) && Object.keys(this._selectedParameters).includes(prop)){
            isMatching = isMatching + this.matchesProp(prop, original, this._selectedParameters)
          }
        }
      } else {
        return 1;
      }
    }
    return isMatching > 0;
  }
  matchesProp(property, originalElement, selectedParameters) {
    return !originalElement || originalElement[property].some(ta => {
      if (!selectedParameters[property]) {
        console.log(selectedParameters, property);
        return false;
      } else {
        return !!(ta && selectedParameters[property].includes(ta));
      }
    })
  }

  calculateUnderwearPerDays() {
    const days = this._textParameters.days;
    const uw = data.underwear;
    uw.map(el => {
      switch (el.id) {
        case 'pants':
        case 'socks':
          el.qty = days;
          break;
        case 'bra':
          // THE BRA FORMULA
          el.qty = Math.ceil(Math.log2(+days + 1));
          break;
        case 'sportBra':
        case 'sportUnderwear':
          el.qty = days === 7 ? 1 : 2;
          break;
        default:
          if (el.type === 'basic')//TODO remember about sports, etc
            el.qty = 1;
      }
      return el;
    });
    this._list = [...this._list, ...uw];
    this.removeDuplicates();
    return this;
  }

  calculateClothesPerDays() {
    const days = this._textParameters.days;
    const seasons = this._selectedParameters.seasons;
    let cl = data.clothes.map(c => {
      switch (c.id) {
        case 'tShirt':
          c.qty = days > 3 ? Math.round(days * 0.8) : days;
          break;
        case 'shirt':
        case 'longSleeve':
          c.qty = days > 3 ? Math.round(days * 0.1) : 0;
          break;
        case 'trousers':
          c.qty = seasons.includes('summer') ? 1 : Math.ceil(Math.log2(+days + 1) / 2);
          break;
        case 'shortTrousers':
          c.qty = seasons.includes('summer') ?
            Math.ceil(Math.log(+days + 1)) :
            seasons.includes('spring') ? 1 : 0;
          break;
        default:
          if (c.type === 'basic') {
            c.qty = 1;
          }
      }
      return c;
    });
    cl = cl.filter(c => c.qty !== 0);
    this._list = [...this._list, ...cl];
    this.removeDuplicates();
    return this;
  }

  customFilter(parameter) {//rm all not matching parameter option arr
    return this._list.filter(el => !el[parameter] || el[parameter].some(ta => {
      if (!this._selectedParameters[parameter]) {
        console.log(this._selectedParameters, parameter);
        return false;
      } else {
        return ta && this._selectedParameters[parameter].includes(ta)
      }
    }));
  }

  filterMatchingItems() {
    this._list = this._list.filter(this.matches.bind(this));
    return this;
  }

  removeByCriteria() {
    Object.keys(this._selectedParameters)
      .forEach(parameter => {
        this._list = this.removeDuplicates(this.customFilter(parameter));
      });
    return this;
  }

  removeByAge() {
    this._list = this._list.filter(el => !el.ageOver || el.ageOver <= this._textParameters.age);
    this._list = this._list.filter(el => !el.ageUnder || el.ageUnder >= this._textParameters.age);
    return this;
  }

  removeDuplicates() {
    this._list = Object.values(_.groupBy(this._list,'id'))
      .map(e=>e.reduce( ( max, cur ) => max.qty>cur.qty ? max : cur, -Infinity ));
    return this;
  }

  groupByCategory() {
    this._list = _.groupBy(this._list, 'category');
    return this;
  }

  toArray() {
    return this._list;
  }

  //legacy
  customFilter2() {//rm all not matching parameter option arr
    this._list = this.removeDuplicates(this._list.filter(this.matches.bind(this)));
    return this;
  }

  removingAlgorithm(){
    return this.calculateUnderwearPerDays()
      .calculateClothesPerDays()
      .removeByCriteria()
      .removeByAge()
      .groupByCategory()
      .toArray();
  }

  matchingAlgorithm() {
    return this.calculateUnderwearPerDays()
      .calculateClothesPerDays()
      .filterMatchingItems()
      .removeDuplicates()
      .removeByAge()
      .groupByCategory()
      .toArray();
  }
}
export class Algorithm {

  constructor(props) {
    this.props = props;
    this.store = store.getState().default;
    this.textOptions = this.store.textOptions;
    this.listItems = this.store.listItems;
    this.initialData = {
      textOptions: this.store.textOptions,
      selectedParameters: this.store.listItems,
      categories: categories
    };
    console.log(this.store.listItems);
    this.list = new Set();

    // TODO: prep packing list for case where there is no data in list
  }

  calculate() {
    if(Object.keys(this.store.list).length){
      this.props.actions.clearList();
    }
    if (this.listItems) {
      let tempList = [];
      categories.forEach(category => {
        switch (category.name) {
          case 'Bielizna':
          case 'Ubrania':
            // skip
            break;
          case 'Motor':
            tempList = tempList.concat(this.handleMotor());
            break;
          case 'Biznes':
            tempList = tempList.concat(this.handleBusiness());
            break;
          default:
            tempList = tempList.concat(this.getAllFromCategory(category));
            break;
        }
      });
      const generatedList = new myList(tempList, this.initialData)
        .matchingAlgorithm();

      console.log(generatedList);
      if (generatedList && Object.values(generatedList).length > 0) {
        this.props.actions.setList(generatedList);
        this.props.navigation.navigate('PackingList');
      } else {
        console.error('List empty or undef')
      }
    } else {
      console.error('Store broken or empty (algorithm.js)')
    }
  }

  getAllFromCategory(category) {
    return data[category.id]
  }

  handleMotor() {
    if (this.listItems.transport && this.listItems.transport.some(el => el === 'Motor' || el === 'motor')) {
      return data.motor;
    }
    return [];
  }

  handleBusiness() {
    if (this.listItems.tripActivities && this.listItems.tripActivities.some(el => el === 'business' || el === 'Biznes')) {
      return data.business;
    }
    return [];
  }

}

function mapStateToProps(state, ownProps) {
  return {store: state}
}

function mapDispatchToProps(dispatch) {
  this.actions = bindActionCreators(Actions, dispatch);
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

connect(mapStateToProps, mapDispatchToProps)(Algorithm);
